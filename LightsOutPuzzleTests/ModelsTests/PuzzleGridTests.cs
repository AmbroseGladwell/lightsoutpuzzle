﻿using Xunit;
using System;

namespace LightsOutPuzzle.Tests
{
    public class PuzzleGridTests
    {
        [Fact]
        public void ToggleLight_ShouldToggleSelectedLight()
        {
            int Row = 1, Col = 1;
            PuzzleGrid PuzzleGrid = new PuzzleGrid();
            bool LightStatus = PuzzleGrid.Grid[Row, Col].On;

            PuzzleGrid.ToggleLight(Row, Col);

            Assert.Equal(!LightStatus, PuzzleGrid.Grid[Row, Col].On);
        }

        [Fact]
        public void ToggleLight_ShouldToggleAdjacentLights()
        {
            int Row = 1, Col = 1;
            PuzzleGrid PuzzleGrid = new PuzzleGrid();
            bool AboveLightStatus = PuzzleGrid.Grid[Row - 1, Col].On;
            bool RightLightStatus = PuzzleGrid.Grid[Row, Col + 1].On;
            bool BottomLightStatus = PuzzleGrid.Grid[Row + 1, Col].On;
            bool LeftLightStatus = PuzzleGrid.Grid[Row, Col - 1].On;

            PuzzleGrid.ToggleLight(Row, Col);

            Assert.Equal(!AboveLightStatus, PuzzleGrid.Grid[Row - 1, Col].On);
            Assert.Equal(!RightLightStatus, PuzzleGrid.Grid[Row, Col + 1].On);
            Assert.Equal(!BottomLightStatus, PuzzleGrid.Grid[Row + 1, Col].On);
            Assert.Equal(!LeftLightStatus, PuzzleGrid.Grid[Row, Col - 1].On);
        }

        [Fact]
        public void IsCoordinateValid_ShouldReturnTrue_ForValidInput()
        {
            int GridSize = 5;
            PuzzleGrid PuzzleGrid = new PuzzleGrid(GridSize);

            Assert.True(PuzzleGrid.IsCoordinateValid(4));
        }

        [Fact]
        public void IsCoordinateValid_ShouldReturnFalse_ForInvalidInput()
        {
            int GridSize = 5;
            PuzzleGrid PuzzleGrid = new PuzzleGrid(GridSize);

            Assert.False(PuzzleGrid.IsCoordinateValid(5));
        }

        [Fact]
        public void AllLightsAreOff_ShouldRaiseEvent_WhenAllGridLightsAreOff()
        {
            bool EventHandlerCalled = false;
            PuzzleGrid PuzzleGrid = CreateWinningTwoByTwoPuzzleState(out int Row, out int Col);

            PuzzleGrid.AllLightsAreOff += delegate (object sender, EventArgs e)
            {
                EventHandlerCalled = true;
            };

            PuzzleGrid.ToggleLight(Row, Col);

            Assert.True(EventHandlerCalled);
        }

        private PuzzleGrid CreateWinningTwoByTwoPuzzleState(out int winningRowIndex, out int winningColIndex)
        {
            int GridSize = 2;
            PuzzleGrid PuzzleGrid = new PuzzleGrid(GridSize);

            int Rank = PuzzleGrid.Grid.Rank;
            int Length = PuzzleGrid.Grid.GetLength(0);

            for (int Row = 0; Row < Rank; Row++)
            {
                for (int Col = 0; Col < Length; Col++)
                {
                    bool On = PuzzleGrid.Grid[Row, Col].On;
                    if((Row.Equals(0) && Col.Equals(0) && !On) || (Row.Equals(0) && Col.Equals(1) && !On) || (Row.Equals(1) && Col.Equals(1) && !On))
                    {
                        PuzzleGrid.Grid[Row, Col].ToggleState();
                    }

                    if(Row.Equals(1) && Col.Equals(0) && On)
                    {
                        PuzzleGrid.Grid[Row, Col].ToggleState();
                    }
                }
            }

            winningRowIndex = 0;
            winningColIndex = 1;
            return PuzzleGrid;
        }
    }
}
