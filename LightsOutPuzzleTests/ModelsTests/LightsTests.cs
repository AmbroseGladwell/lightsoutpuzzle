using Xunit;

namespace LightsOutPuzzle.Tests
{
    public class LightTests
    {
        [Fact]
        public void ToggleState_ShouldToggleOnProp()
        {
            bool On = true;
            Light Light = new Light(On);

            Light.ToggleState();

            Assert.False(Light.On);
        }
    }
}
