﻿using System;

namespace LightsOutPuzzle
{
    public class PuzzleManager
    {
        private const string Exit = "exit";

        public PuzzleManager()
        {
            Puzzle = new PuzzleGrid();
            Puzzle.AllLightsAreOff += C_OnGameComplete;
            GameComplete = false;
            ExitGame = false;
        }

        private PuzzleGrid Puzzle { get; set; }

        private bool GameComplete { get; set; }

        private bool ExitGame { get; set; }

        public void StartPuzzle()
        {
            String Result;
            DisplayWelcomeMessage();
            DisplayBeforeGameInstructions();

            do
            {
                Result = ReadLine.Read("Are you ready? Enter Y to start or N to exit: ");
            } while (!Result.Equals("y", StringComparison.OrdinalIgnoreCase) && !Result.Equals("n", StringComparison.OrdinalIgnoreCase));

            if (Result.Equals("n", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            do
            {
                Console.Clear();

                DisplayWelcomeMessage();
                DisplayInGameInstructions();
                Puzzle.DisplayGrid();

                bool ShouldContinue = GetSelectedLight(out int Row, out int Col);

                if (!ShouldContinue)
                {
                    break;
                }

                Puzzle.ToggleLight(Row, Col);
            } while (!GameComplete || !ExitGame);

            if (GameComplete)
            {
                DisplayGameCompletedMessage();
            }
            else if (ExitGame)
            {
                DisplayGameExitedMessage();
            }
        }

        protected void C_OnGameComplete(object sender, EventArgs e)
        {
            GameComplete = true;
        }

        private void DisplayWelcomeMessage()
        {
            Console.WriteLine("Welcome to Lights Out Puzzle");
            Console.WriteLine("----------------------------");
            Console.WriteLine();
        }

        private void DisplayInGameInstructions()
        {
            Console.WriteLine("Enter the row or column number after the prompt and press Enter to toggle a light.");
            Console.WriteLine("Enter EXIT after any prompt and press Enter to quit the game.");
            Console.WriteLine();
            Console.WriteLine("----------------------------");
            Console.WriteLine();
        }

        private void DisplayBeforeGameInstructions()
        {
            Console.WriteLine("Lights Out is a puzzle game consisting of an 5x5 grid of lights. At the beginning of the game, some of the lights are switched on. When a light is selected by inputing the light row and column coordinates, this light and the four adjacent lights (above, right, bottom and left) are toggled. The purpose of the game is to switch all the lights off.");
            Console.WriteLine();
            Console.WriteLine("Key");
            Console.WriteLine("----");
            Console.WriteLine("* = Light is on");
            Console.WriteLine("• = Light is off");
            Console.WriteLine();
            Console.WriteLine("Below is the puzzle grid with lights on (*) and off (•)");
            Console.WriteLine();
            Puzzle.DisplayGrid();
            Console.WriteLine("Good luck!");
            Console.WriteLine();
        }

        private bool GetSelectedLight(out int row, out int col)
        {
            Console.WriteLine("Select your next light to toggle...");

            row = ReadAndValidateNextMove("Enter the row number: ");

            if (ExitGame)
            {
                col = 0;
                return false;
            }

            col = ReadAndValidateNextMove("Enter the column number: ");

            if (ExitGame)
            {
                return false;
            }

            return true;
        }

        private void DisplayGameCompletedMessage()
        {
            Console.WriteLine();
            Console.WriteLine("Whoohoo!!! You did it! Give yourself a pat on the back!");
        }

        private void DisplayGameExitedMessage()
        {
            Console.WriteLine();
            Console.WriteLine("Sorry to see you go so soon, better luck next time.");
        }

        private int ReadAndValidateNextMove(string message)
        {
            String Result = ReadLine.Read(message);

            int NewCoordinate;
            while (!IsValidInput(Result, out NewCoordinate))
            {
                Result = ReadLine.Read("Invalid row or column number, try again: ");
            }

            return NewCoordinate;
        }

        private bool IsValidInput(string result, out int coordinate)
        {
            if (Int32.TryParse(result, out int NewCoordinate))
            {
                if (Puzzle.IsCoordinateValid(NewCoordinate))
                {
                    coordinate = NewCoordinate;
                    return true;
                }
            }
            else if (result.Equals(Exit, StringComparison.OrdinalIgnoreCase))
            {
                coordinate = NewCoordinate;
                ExitGame = true;
                return true;
            }

            coordinate = NewCoordinate;
            return false;
        }
    }
}
