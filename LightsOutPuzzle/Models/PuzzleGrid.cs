﻿using System;
using System.Collections.Generic;

namespace LightsOutPuzzle
{
    public class PuzzleGrid
    {
        private const int DefaultGridSize = 5;
        private const int RowAndColHeaderIndex = -1;
        private const int RowAndColStartIndex = 0;

        public PuzzleGrid(int gridSize = DefaultGridSize)
        {
            GridSize = gridSize;
            InitialisePuzzleGrid();
        }

        public event EventHandler AllLightsAreOff;

        public Light[,] Grid { get; private set; }

        protected int GridSize { get; set; }

        public void DisplayGrid()
        {
            for (int Row = RowAndColHeaderIndex; Row < GridSize; Row++)
            {
                Console.Write(" ");

                for (int Col = RowAndColHeaderIndex; Col < GridSize; Col++)
                {
                    if(Row == RowAndColHeaderIndex && Col == RowAndColHeaderIndex)
                    {
                        Console.Write(" ");
                    } else if(Row != RowAndColHeaderIndex && Col == RowAndColHeaderIndex)
                    {
                        Console.Write(Row);
                    } else if(Row == RowAndColHeaderIndex && Col != RowAndColHeaderIndex)
                    {
                        Console.Write(Col);
                    } else
                    {
                        Console.Write(Grid[Row, Col].On ? "*" : "•");
                    }

                    Console.Write(" ");
                }

                Console.WriteLine();
            }

            Console.WriteLine("\n");
        }

        public void ToggleLight(int row, int col)
        {
            if (IsCoordinateValid(row) && IsCoordinateValid(col))
            {
                List<bool> UpdatedLightStatuses = new List<bool>();
                UpdatedLightStatuses.Add(Grid[row, col].ToggleState());
                ToggleAdjacentLights(row, col, UpdatedLightStatuses);
            }
        }

        public bool IsCoordinateValid(int coordinate)
        {
            return coordinate >= RowAndColStartIndex && coordinate < GridSize;
        }

        protected virtual void OnAllLightsAreOff(EventArgs e)
        {
            AllLightsAreOff?.Invoke(this, e);
        }

        private void InitialisePuzzleGrid()
        {
            bool IsOneLightOn = false;

            Grid = new Light[GridSize, GridSize];
            for (int Row = RowAndColStartIndex; Row < GridSize; Row++)
            {
                for (int Col = RowAndColStartIndex; Col < GridSize; Col++)
                {
                    bool On = GenerateRandomBoolean();

                    if (Row == GridSize - 1 && Col == GridSize - 1)
                    {
                        On = !IsOneLightOn || On;
                    }

                    Grid[Row, Col] = new Light(On);
                }
            }
        }

        private void ToggleAdjacentLights(int row, int col, List<bool> updatedLightStatuses)
        {
            if (IsCoordinateValid(col - 1))
            {
                updatedLightStatuses.Add(Grid[row, col - 1].ToggleState());
            }

            if (IsCoordinateValid(row + 1))
            {
                updatedLightStatuses.Add(Grid[row + 1, col].ToggleState());
            }

            if (IsCoordinateValid(col + 1))
            {
                updatedLightStatuses.Add(Grid[row, col + 1].ToggleState());
            }

            if (IsCoordinateValid(row - 1))
            {
                updatedLightStatuses.Add(Grid[row - 1, col].ToggleState());
            }

            if (!updatedLightStatuses.Contains(true))
            {
                AreAllLightsOff();
            }
        }

        private void AreAllLightsOff()
        {
            bool OnFound = false;

            for (int Row = RowAndColStartIndex; Row < GridSize; Row++)
            {
                for (int Col = RowAndColStartIndex; Col < GridSize; Col++)
                {
                    OnFound = Grid[Row, Col].On;
                    if (OnFound)
                    {
                        break;
                    }
                }

                if (OnFound)
                {
                    break;
                }
            }

            if (!OnFound)
            {
                OnAllLightsAreOff(EventArgs.Empty);
            }
        }

        private bool GenerateRandomBoolean()
        {
            Random Random = new Random();
            double Divider = 1.1;
            int TotalLights = GridSize * GridSize;
            return Random.Next(TotalLights) > (TotalLights / Divider);
        }
    }
}