﻿namespace LightsOutPuzzle
{
    public class Light
    {
        public Light(bool on)
        {
            On = on;
        }

        public bool On { get; private set; }

        public bool ToggleState()
        {
            On = !On;
            return On;
        }
    }
}
