## Lights out puzzle

Lights Out is a puzzle game consisting of an 5x5 grid of lights. At the beginning of the game, some of the lights are switched on. When a light is pressed or selected, this light and the four adjacent lights are toggled, i.e., they are switched on if they were off, and switched off otherwise. The purpose of the game is to switch all the lights off.
 
 The game must start with some lights turned on, and the user keeps playing till they either give up or turn off all the lights.

---

## Implementation

This implementation of the Lights out puzzle is a C# console application built on .NET Core 2.1.